#pragma once

#include "OVR.h"
#include <msclr\auto_gcroot.h>
#include "ScopedPtr.h"

namespace OculusSharp {

	ref class DeviceBase;
	ref class Profile;

	ref class ProfileManager
	{
	protected:
		OVR::ProfileManager* impl;
	public:
		ProfileManager(OVR::ProfileManager* impl) { this->impl = impl; impl->AddRef(); }
		~ProfileManager() { delete this; }
		!ProfileManager() { if (impl != NULL) impl->Release(); impl = NULL; }
		OVR::ProfileManager* Native() { return impl; }

		static System::String^ KEY_USER = "User";
		static System::String^ KEY_NAME = "Name";
		static System::String^ KEY_GENDER = "Gender";
		static System::String^ KEY_PLAYER_HEIGHT = "PlayerHeight";
		static System::String^ KEY_EYE_HEIGHT = "EyeHeight";
		static System::String^ KEY_IPD = "IPD";
		static System::String^ KEY_NECK_TO_EYE_DISTANCE = "NeckEyeDistance";
		static System::String^ KEY_EYE_RELIEF_DIAL = "EyeReliefDial";
		static System::String^ KEY_EYE_TO_NOSE_DISTANCE = "EyeToNoseDist";
		static System::String^ KEY_MAX_EYE_TO_PLATE_DISTANCE = "MaxEyeToPlateDist";
		static System::String^ KEY_EYE_CUP = "EyeCup";
		static System::String^ KEY_CUSTOM_EYE_RENDER = "CustomEyeRender";
		static System::String^ DEFAULT_GENDER = "Male";
		static float DEFAULT_PLAYER_HEIGHT = 1.778f;
		static float DEFAULT_EYE_HEIGHT = 1.675f;
		static float DEFAULT_IPD = 0.064f;
		static float DEFAULT_NECK_TO_EYE_HORIZONTAL = 0.09f;
		static float DEFAULT_NECK_TO_EYE_VERTICAL = 0.15f;
		static int DEFAULT_EYE_RELIEF_DIAL = 3;

		static ProfileManager^ Create();
		int GetUserCount();
		System::String^ GetUser(unsigned int index);
		bool CreateUser(System::String^ user, System::String^ name);
		bool RemoveUser(System::String^ user);
		System::String^ GetDefaultUser(DeviceBase^ device);
		bool SetDefaultUser(DeviceBase^ device, System::String^ user);
		virtual Profile^ CreateProfile();
		Profile^ GetProfile(DeviceBase^ device, System::String^ user);
		Profile^ GetDefaultProfile(DeviceBase^ device);
		Profile^ GetTaggedProfile(System::Collections::Generic::Dictionary<System::String^, System::String^>^ keys);
		bool SetTaggedProfile(System::Collections::Generic::Dictionary<System::String^, System::String^>^ keys, Profile^ profile);
		bool GetDeviceTags(DeviceBase^ device, System::String^% product, System::String^% serial);
	};


	ref class Profile
	{
	protected:
		OVR::Profile* impl;

	public:
		Profile(OVR::Profile* impl) { this->impl = impl; impl->AddRef(); }
		~Profile() { delete this; }
		!Profile() { if (impl != NULL) impl->Release(); impl = NULL; }
		OVR::Profile* Native() { return impl; }

		int                 GetNumValues(System::String^ key);
		System::String^     GetValue(System::String^ key);
		System::String^     GetValue(System::String^ key, System::String^ val, int val_length);
		bool                GetBoolValue(System::String^ key, bool default_val);
		int                 GetIntValue(System::String^ key, int default_val);
		float               GetFloatValue(System::String^ key, float default_val);
		cli::array<float>^  GetFloatValues(System::String^ key, int num_vals);
		double              GetDoubleValue(System::String^ key, double default_val);
		cli::array<double>^ GetDoubleValues(System::String^ key, int num_vals);

		void                SetValue(System::String^ key, System::String^ val);
		void                SetBoolValue(System::String^ key, bool val);
		void                SetIntValue(System::String^ key, int val);
		void                SetFloatValue(System::String^ key, float val);
		void                SetFloatValues(System::String^ key, cli::array<float>^ vals);
		void                SetDoubleValue(System::String^ key, double val);
		void                SetDoubleValues(System::String^ key, cli::array<double>^ vals);

		bool Close();
	};

}