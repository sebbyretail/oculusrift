#pragma once

#include "OVR.h"
#include "OVR_Device.h"

#define VEC3(a) gcnew SharpDX::Vector3((a).x, (a).y, (a).z)

namespace OculusSharp {

	ref class Message
	{
	protected:
		clr_scoped_ptr<OVR::Message> impl;
	public:
		Message() { impl = new OVR::Message(); }
		Message(OVR::Message& info) { impl = new OVR::Message(info); }
		OVR::Message* Native() { return impl.get(); }
		property MessageType Type { MessageType get() { return (MessageType)impl.get()->Type; } }
		property DeviceBase^ Device { DeviceBase^ get() { return gcnew DeviceBase(impl.get()->pDevice); } }
	};

	ref class MessageBodyFrame : Message
	{
	protected:
	public:
		MessageBodyFrame(OVR::MessageBodyFrame& info) { impl = new OVR::MessageBodyFrame(info); }
		OVR::MessageBodyFrame* Native() { return (OVR::MessageBodyFrame*)impl.get(); }
		property float Temperature { float get() { return ((OVR::MessageBodyFrame*)impl.get())->Temperature; } }
		property float TimeDelta { float get() { return ((OVR::MessageBodyFrame*)impl.get())->TimeDelta; } }
		property double AbsoluteTimeSeconds { double get() { return ((OVR::MessageBodyFrame*)impl.get())->AbsoluteTimeSeconds; } }
		property SharpDX::Vector3^ Acceleration { SharpDX::Vector3^ get() { return VEC3(((OVR::MessageBodyFrame*)impl.get())->Acceleration); } }
		property SharpDX::Vector3^ RotationRate { SharpDX::Vector3^ get() { return VEC3(((OVR::MessageBodyFrame*)impl.get())->RotationRate); } }
		property SharpDX::Vector3^ MagneticField { SharpDX::Vector3^ get() { return VEC3(((OVR::MessageBodyFrame*)impl.get())->MagneticField); } }
	};

	ref class MessageDeviceStatus : Message
	{
	protected:
	public:
		MessageDeviceStatus(OVR::MessageDeviceStatus& info) : Message(info) { impl = new OVR::MessageDeviceStatus(info); }
		OVR::MessageDeviceStatus* Native() { return (OVR::MessageDeviceStatus*)impl.get(); }
		property DeviceHandle^ Handle { DeviceHandle^ get() { return gcnew DeviceHandle(((OVR::MessageDeviceStatus*)impl.get())->Handle); } }
	};

	ref class MessageExposureFrame : Message
	{
	protected:
	public:
		MessageExposureFrame(OVR::MessageExposureFrame& info) { impl = new OVR::MessageExposureFrame(info); }
		OVR::MessageExposureFrame* Native() { return (OVR::MessageExposureFrame*)impl.get(); }
		property double CameraTimeSeconds { double get() { return ((OVR::MessageExposureFrame*)impl.get())->CameraTimeSeconds; } }
		property byte CameraPattern { byte get() { return ((OVR::MessageExposureFrame*)impl.get())->CameraPattern; } }
		property UInt32 CameraFrameCount { UInt32 get() { return ((OVR::MessageExposureFrame*)impl.get())->CameraFrameCount; } }
	};

	ref class MessagePixelRead : Message
	{
	protected:
	public:
		MessagePixelRead(OVR::MessagePixelRead& info) { impl = new OVR::MessagePixelRead(info); }
		OVR::MessagePixelRead* Native() { return (OVR::MessagePixelRead*)impl.get(); }
		property double SensorTimeSeconds { double get() { return ((OVR::MessagePixelRead*)impl.get())->SensorTimeSeconds; } }
		property double FrameTimeSeconds { double get() { return ((OVR::MessagePixelRead*)impl.get())->FrameTimeSeconds; } }
		property byte PixelReadValue { byte get() { return ((OVR::MessagePixelRead*)impl.get())->PixelReadValue; } }
		property UInt32 RawSensorTime { UInt32 get() { return ((OVR::MessagePixelRead*)impl.get())->RawSensorTime; } }
		property UInt32 RawFrameTime { UInt32 get() { return ((OVR::MessagePixelRead*)impl.get())->RawFrameTime; } }
	};

	ref class MessageLatencyTestSamples : Message
	{
	protected:
	public:
		MessageLatencyTestSamples(OVR::MessageLatencyTestSamples& info) { impl = new OVR::MessageLatencyTestSamples(info); }
		OVR::MessageLatencyTestSamples* Native() { return (OVR::MessageLatencyTestSamples*)impl.get(); }
		property cli::array<SharpDX::ColorBGRA>^ Samples 
		{ 
			cli::array<SharpDX::ColorBGRA>^ get()
			{
				cli::array<SharpDX::ColorBGRA>^ res = gcnew cli::array<SharpDX::ColorBGRA>(Native()->Samples.GetSize());
				for (int idx = 0; idx < res->Length; idx++)
				{
					res[idx] = SharpDX::ColorBGRA(Native()->Samples[idx].R, Native()->Samples[idx].G, Native()->Samples[idx].B, Native()->Samples[idx].A);
				}
				return res;
			}
		}
	};


	ref class MessageLatencyTestColorDetected : Message
	{
	protected:
	public:
		MessageLatencyTestColorDetected(OVR::MessageLatencyTestColorDetected& info) { impl = new OVR::MessageLatencyTestColorDetected(info); }
		OVR::MessageLatencyTestColorDetected* Native() { return (OVR::MessageLatencyTestColorDetected*)impl.get(); }
		property UInt16 Elapsed { UInt16 get() { return ((OVR::MessageLatencyTestColorDetected*)impl.get())->Elapsed; } }
		property SharpDX::ColorBGRA DetectedValue { SharpDX::ColorBGRA get() { return SharpDX::ColorBGRA(Native()->DetectedValue.R, Native()->DetectedValue.G, Native()->DetectedValue.B, Native()->DetectedValue.A); } }
		property SharpDX::ColorBGRA TargetValue { SharpDX::ColorBGRA get() { return SharpDX::ColorBGRA(Native()->TargetValue.R, Native()->TargetValue.G, Native()->TargetValue.B, Native()->TargetValue.A); } }
	};

	ref class MessageLatencyTestStarted : Message
	{
	protected:
	public:
		MessageLatencyTestStarted(OVR::MessageLatencyTestStarted& info) { impl = new OVR::MessageLatencyTestStarted(info); }
		OVR::MessageLatencyTestStarted* Native() { return (OVR::MessageLatencyTestStarted*)impl.get(); }
		property SharpDX::ColorBGRA TargetValue { SharpDX::ColorBGRA get() { return SharpDX::ColorBGRA(Native()->TargetValue.R, Native()->TargetValue.G, Native()->TargetValue.B, Native()->TargetValue.A); } }
	};

	ref class MessageLatencyTestButton : Message
	{
	protected:
	public:
		MessageLatencyTestButton(OVR::MessageLatencyTestButton& info) { impl = new OVR::MessageLatencyTestButton(info); }
		OVR::MessageLatencyTestButton* Native() { return (OVR::MessageLatencyTestButton*)impl.get(); }
	};

	////-------------------------------------------------------------------------------------
	//// ***** Camera

	ref class MessageCameraFrame : Message
	{
	protected:
	public:
		MessageCameraFrame(OVR::MessageCameraFrame& info) { impl = new OVR::MessageCameraFrame(info); }
		OVR::MessageCameraFrame* Native() { return (OVR::MessageCameraFrame*)impl.get(); }
		property double ArrivalTimeSeconds { double get() { return ((OVR::MessageCameraFrame*)impl.get())->ArrivalTimeSeconds; } }
		property UInt32 FrameNumber { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->FrameNumber; } }
		property UInt32 FrameSizeInBytes { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->FrameSizeInBytes; } }
		property UInt32 Width { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->Width; } }
		property UInt32 Height { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->Height; } }
		property UInt32 Format { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->Format; } }
		property UInt32 LostFrames { UInt32 get() { return ((OVR::MessageCameraFrame*)impl.get())->LostFrames; } }
		property IntPtr CameraHandle { IntPtr get() { return (IntPtr)((OVR::MessageCameraFrame*)impl.get())->CameraHandle; } }
		property System::String^ DeviceIdentifier { System::String^ get() { return gcnew System::String(((OVR::MessageCameraFrame*)impl.get())->DeviceIdentifier); } }
		property cli::array<byte>^ FrameData
		{
			cli::array<byte>^ get()
			{
				cli::array<byte>^ res = gcnew cli::array<byte>(Native()->FrameSizeInBytes);
				for (int idx = 0; idx < res->Length; idx++)
					res[idx] = Native()->pFrameData[idx];
				return res;
			}
		}
	};

	ref class MessageCameraAdded : Message
	{
	protected:
	public:
		MessageCameraAdded(OVR::MessageCameraAdded& info) { impl = new OVR::MessageCameraAdded(info); }
		OVR::MessageCameraAdded* Native() { return (OVR::MessageCameraAdded*)impl.get(); }
		property IntPtr CameraHandle { IntPtr get() { return (IntPtr)Native()->CameraHandle; } }
	};
	

}