// This is the main DLL file.

#include "stdafx.h"

namespace OculusSharp {

	// ---------------------------------------------------------
	// --- DeviceHandle
	DeviceHandle::DeviceHandle(OVR::DeviceHandle& impl) { this->impl = new OVR::DeviceHandle(impl); }
	DeviceHandle::DeviceHandle(DeviceHandle^ src) { this->impl = new OVR::DeviceHandle(*src->impl); }
	void DeviceHandle::operator = (DeviceHandle^ src) { this->impl = new OVR::DeviceHandle(*src->impl); }
	bool DeviceHandle::operator == (DeviceHandle^ other) { return impl == other->impl; }
	bool DeviceHandle::operator != (DeviceHandle^ other) { return impl != other->impl; }
	DeviceHandle::operator bool() { return impl.get()->GetType() != OVR::Device_None; }
	DeviceBase^ DeviceHandle::GetDevice_AddRef() { return gcnew DeviceBase(impl.get()->GetDevice_AddRef()); }
	DeviceType DeviceHandle::GetType() { return (DeviceType)(impl.get()->GetType()); }
	DeviceInfo^ DeviceHandle::GetDeviceInfo() { OVR::DeviceInfo info; impl.get()->GetDeviceInfo(&info); return gcnew DeviceInfo(info); }
	bool        DeviceHandle::IsAvailable() { return (impl.get()->IsAvailable()); }
	bool        DeviceHandle::IsCreated() { return (impl.get()->IsCreated()); }
	bool        DeviceHandle::IsDevice(DeviceBase^ device) { return impl.get()->IsDevice(device->Native()); }
	DeviceBase^ DeviceHandle::CreateDevice() { return gcnew DeviceBase(impl.get()->CreateDevice()); }
	void		DeviceHandle::Clear() { impl.get()->Clear(); }

}
